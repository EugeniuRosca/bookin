package com.example.bookin;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class Menu extends Activity{
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.menu);
		TextView loggedinas = (TextView)findViewById(R.id.menu_user);
		loggedinas.setText("Logged in as: "+User.name);
	}
	
	public void NewBookin_function(View v){
	    Intent intent = new Intent(this, New_bookin.class);
	    Menu.this.startActivity(intent);
	}
	
	public void SearchBookin_function(View v){
	    Intent intent = new Intent(this, Site_search.class);
	    Menu.this.startActivity(intent);
	}
	
	public void ChangePassword_function(View v){
	    Intent intent = new Intent(this, Change_password.class);
	    Menu.this.startActivity(intent);
	}
	
	public void SignOut_function(View v) {
		this.finish();
		// TODO: on sign-out button press, implement dialog "Sign out?" 
	}
	
	public void MyBookins_function(View v){
	    Intent intent = new Intent(this, My_bookins.class);
	    Menu.this.startActivity(intent);
	}
	
	public void menu_function(View v){
		setContentView(R.layout.menu);
	}
	
	@Override
	public void onBackPressed() {
		//TODO: on back-button press, implement dialog "Sign out?" 
		Toast.makeText(this, "You signed off!", Toast.LENGTH_SHORT).show();
		this.finish();
	}

	
//	@Override
//	public void onBackPressed() {
//		//TODO: on back-button press, implement dialog "Sign out?" 
//	}
}