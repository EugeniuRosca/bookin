package com.example.bookin;

import java.util.List;

import org.apache.http.NameValuePair;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.CountDownTimer;

class AsyncParam {
	Context context;
	String url;
	List<NameValuePair> msg;
	ConnectionType connType;
	AsyncParam (Context context, String url, List<NameValuePair> srvMsg, ConnectionType type) {
        this.context = context;
        this.msg = srvMsg;
        this.url = url;
        this.connType = type;
    }
}

public class AsyncTaskCommon extends AsyncTask<AsyncParam, Void, String> {
	private Activity activity;
	private AsyncTaskCallbacks callback;
	private static final int MIN_DELAY = 200; // milliseconds
	private final ProgressDialog progressDialog;
	private final CountDownTimer countDownTimer;

	public AsyncTaskCommon(Context ctx) {
		this.callback = (AsyncTaskCallbacks)ctx;
		this.activity = (Activity)ctx;
		this.progressDialog = this.createProgressDialog(activity);
		this.countDownTimer = this.createCountDownTimer();
	}
	
	private ProgressDialog createProgressDialog(Activity activity) {
		final ProgressDialog progressDialog = new ProgressDialog(activity);
		progressDialog.setIndeterminate(true);
		return progressDialog;
	}
	
	private CountDownTimer createCountDownTimer() {
		return new CountDownTimer(MIN_DELAY, MIN_DELAY + 1) {
			@Override public void onTick(long millisUntilFinished) { }
			@Override public void onFinish() {
				progressDialog.show();
				progressDialog.setMessage("Loading...");
			}
		};
	}

	protected void onPreExecute() {
		super.onPreExecute();
		countDownTimer.start();
	}

	protected String doInBackground(AsyncParam... param) {
		String result = null;
		ConnectionState state = (new Connection(param[0].context)).check();
		if (state == ConnectionState.CONNECTION_OK)
			result = null;
		else if(state == ConnectionState.INTERNET_DOWN)
			result = "No connection to internet";
		else if(state == ConnectionState.SERVER_DOWN)
			result = "No connection to server";
		else
			throw new RuntimeException("CommonAsyncTask.doInBackground: UNHANDLED ConnectionState !");

		if(result != null)
			return result;
		else
			return this.callback.doInBg(param[0]);
	}
	
	protected void onPostExecute(String param) {
		super.onPostExecute(param);
		countDownTimer.cancel();
		this.callback.postExec(param);
		if(progressDialog.isShowing())
			progressDialog.dismiss();
	}
}