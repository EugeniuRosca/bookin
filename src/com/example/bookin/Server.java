package com.example.bookin;

import java.util.HashMap;
import java.util.List;

import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;

class ServerReply {
	String error;
	HashMap<String, String> info;
	ServerReply (String err, HashMap<String, String> map) {
        this.error = err;
        this.info = map;
    }
}

public class Server {

	public static void startAsync(Context ctx, List<NameValuePair> p, String url, ConnectionType type) {
		AsyncParam param = new AsyncParam(ctx, url, p, type);
		new AsyncTaskCommon(ctx).execute(param);
	}
	
	public static ServerReply sendReceive(String url, List<NameValuePair> params, ConnectionType type) {
		String error = null;
		int success = 0;
		JSONObject obj = null;
		JSONArray array;
		HashMap<String, String> map = new HashMap<String, String>();
		
		switch (type) {
			case CHANGE_PASSWORD:
			case MAIN_LOGIN:
			case NEW_BOOKIN:
			case SIGN_UP:
				obj = Json.getObject(url, params);
				if(obj == null) {
					error = "ERROR: Http connection issue";
					break;
				}
				try {
					success = obj.getInt(Tag.SUCCESS); 
				} catch (JSONException e) {
					error = "ERROR: Protocol issue";
					break;
				}
				if(success == 1)
					break; // skip the next steps
				// Error is specific to each connectionType
				switch (type) {
					case CHANGE_PASSWORD: error = "Password not changed!"; break;
					case MAIN_LOGIN:	  error = "Wrong credentials!"; break;
					case NEW_BOOKIN:	  error = "New bookin rejected by server!"; break;
					case SIGN_UP:		  error = "Failed to register new user!"; break;
					default:			  error = "Rejected by server!"; break;
				}
				break;
				
			case MY_BOOKINS:
			case SITE_SEARCH:
				array = Json.getArray(url, params);
				if(array == null) {
					error = "ERROR: Http connection issue";
					break;
				}
				break;
				
			case USER_DETAILS:
				array = Json.getArray(url, params);
				if(array == null) {
					error = "ERROR: Http connection issue";
					break;
				}
				try {
					obj = array.getJSONObject(0);
					//success = obj.getInt(Tag.SUCCESS); // PHP does not define 'success' field !
					if(obj == null) {
						error = "Request rejected by server";
						break;
					}
					String company = obj.getString(Tag.COMPANY);
					String phone = obj.getString(Tag.PHONE);
					map.put(Tag.COMPANY, company);
					map.put(Tag.PHONE, phone);
				} catch (JSONException e) {
					e.printStackTrace();
					error = "ERROR: Protocol issue";
				}
				break;
		}
		
		// UPDATE some global variables based on data received from server 
		if((type == ConnectionType.SIGN_UP) && (success == 1)) {
			try {
				User.name = obj.getString(Tag.NEWUSER);
			} catch (JSONException e) {
				error = "ERROR: Unable to decode server data";
			}
		}
		return new ServerReply(error, map);
	}
}