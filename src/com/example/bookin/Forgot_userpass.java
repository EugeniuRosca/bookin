package com.example.bookin;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;

public class Forgot_userpass extends Activity{
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.forgot_userpass);
		this.printMessage("Autorecovery feature"+'\n'+"not available yet...");
	}
	
	//sa verifice daca userul sau adresa de email data pentru recuperarea parolei sunt valide
	//sa trimita email/sms cu userul si parola pt bookin app	
	public void recover_up_function(View v) {
		this.hideKeyboard();
		this.printToast("Contact the system administrator!");
	}
	
	private void hideKeyboard() {
		InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
 		imm.hideSoftInputFromWindow(getWindow().getCurrentFocus().getWindowToken(), 0);
	}
	
	private void printMessage(String msg) {
		TextView message = (TextView)findViewById(R.id.forgot_message);
		message.setText(msg);
	}
	
	private void printToast(String msg) {
		Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
	}
}
