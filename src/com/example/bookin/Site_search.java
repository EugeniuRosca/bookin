package com.example.bookin;

import java.util.ArrayList;
import java.util.List;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

public class Site_search extends Activity implements AsyncTaskCallbacks {

	private static int showToast;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.site_search);
		showToast=0;
	}
	
	@SuppressLint({ "NewApi", "ShowToast" })
	public void sitesearch_search_function(View v) {
		EditText sitenr = (EditText)findViewById(R.id.sitesearch_sitenr);
		String site = sitenr.getText().toString();
		
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("Opstelpunt", site));
		Server.startAsync(this, params, Url.sitesearch, ConnectionType.SITE_SEARCH);
		
		
	}

	private void printResultCount(int num) {
		TextView nr_of_results = (TextView)findViewById(R.id.sitesearch_nr_of_results);
		if(num == 1)
			nr_of_results.setText("1 result");
		else
			nr_of_results.setText(num + " results");			
	}
	
	private void hideKeyboard() {
		InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
 		imm.hideSoftInputFromWindow(getWindow().getCurrentFocus().getWindowToken(), 0);
	}
	
	private void printToast(String msg) {
		Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
	}
	
	private void printTableEntries() {
		TableLayout searchtable = (TableLayout) findViewById(R.id.sitesearch_table);
		searchtable.removeAllViews();
		if(Json.arrayLength() == 0)
			this.printToast("No records found!");
		else
			Json.printArray(this, searchtable);
	}
	
	public String doInBg(AsyncParam p) {
		ServerReply reply = Server.sendReceive(p.url, p.msg, p.connType);
		return reply.error;
	}
	
	public void postExec(String error) {
		Log.i("postExec_Site_search", "entered with param: "+error);
		this.hideKeyboard();
		if(error != null) {
			this.printToast(error);
			return;
		}
		if(showToast == 0) {
			showToast = 1;
			printToast("Click on items to show/hide details");
		}
		this.printResultCount(Json.arrayLength());
		this.printTableEntries();
		Log.i("postExec_Site_search", "exited");
	}
}