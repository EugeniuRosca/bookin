package com.example.bookin;

class Url {
	public static final String ip = "http://188.26.248.155";
	public static final String server = ip+":8888/";
	public static final String login = ip+":8888/php/login.php";
	public static final String mybookins = ip+":8888/php/mybookins.php";
	public static final String mynewopenbookins = ip+":8888/php/mynewopenbookins.php";
	public static final String myallbookins = ip+":8888/php/myallbookins.php";
	public static final String newbookin = ip+":8888/php/newbookin.php";
	public static final String sitesearch = ip+":8888/php/sitesearch.php";
	public static final String changepassword = ip+":8888/php/changepassword.php";
	public static final String newuser = ip+":8888/php/newuser.php";
	public static final String userdetails = ip+":8888/php/userdetails.php";
}

class User {
	public static String name;
	public static String pass;
	public static String remember;
	
}

class Tag {
	public static final String SUCCESS = "success";
	public static final String NEWUSER = "message";
	public static final String PHONE = "phone";
	public static final String COMPANY = "company";
}