package com.example.bookin;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class Signup extends Activity implements AsyncTaskCallbacks {
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.signup);
	}

	public void signup_function(View v) {
		String firstname = ((EditText) findViewById(R.id.signup_firstname)).getText().toString();
		String lastname  = ((EditText)findViewById(R.id.signup_lastname)).getText().toString();
		String company 	 = ((EditText)findViewById(R.id.signup_company)).getText().toString();
		String email 	 = ((EditText)findViewById(R.id.signup_email)).getText().toString();
		String phone 	 = ((EditText)findViewById(R.id.signup_phone)).getText().toString();
		String pass 	 = ((EditText)findViewById(R.id.signup_password)).getText().toString();
		String pass2 	 = ((EditText)findViewById(R.id.signup_password2)).getText().toString();
		
		if((firstname.matches("")) || (lastname.matches("")) || (company.matches("")) || 
			(email.matches(""))    || (phone.matches(""))    || (pass.matches("")) ) {
			TextView alert = (TextView)findViewById(R.id.signup_alert);
			alert.setTextColor(Color.RED);
			return;
		}
		if(!pass2.equals(pass)) {
			this.printToast("Password doesn't match!");
			return;
		}
		
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("firstname", firstname));
		params.add(new BasicNameValuePair("lastname", lastname));
		params.add(new BasicNameValuePair("company", company));
		params.add(new BasicNameValuePair("email", email));
		params.add(new BasicNameValuePair("phone", phone));
		params.add(new BasicNameValuePair("passcode", pass));
		
		Server.startAsync(this, params, Url.newuser, ConnectionType.SIGN_UP);
	}
	
	private void hideKeyboard() {
		InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
 		imm.hideSoftInputFromWindow(getWindow().getCurrentFocus().getWindowToken(), 0);
	}
	
	private void printToast(String msg) {
		Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
	}
	
	public String doInBg(AsyncParam p) {
		ServerReply reply = Server.sendReceive(p.url, p.msg, p.connType); 
		return reply.error;
	}
	
	private void showConfirmation() {
		new AlertDialog.Builder(this)
	    .setTitle("Welcome!")
	    .setMessage("Your username is: "+User.name)
	    .setCancelable(false)
	    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
	        public void onClick(DialogInterface dialog, int which) { 
			    finish();
	        }
	     })
	     //.setIcon(android.R.drawable.ic_dialog_alert)
	     .show();
	}
	
	public void postExec(String error) {
		Log.i("postExec_Signup", "entered with param: "+error);
		this.hideKeyboard();
		if(error != null) {
			this.printToast(error);
		}
		else {
			// At this moment User.name is already updated by Server class
			this.showConfirmation();
		}
		Log.i("postExec_Signup", "exited");
	}
}