package com.example.bookin;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

public class Json {
	private static JSONArray jArray = null;

	// Convention: return null on error, return JSONObject on success
	public static JSONObject getObject(String url, List<NameValuePair> params) {
		String json = httpRequest(url, params);
		if(json == null) { return null; }
		try { return (new JSONObject(json)); }
		catch (JSONException e) {
			Log.e("Json.getObject", "Error: " +e.toString());
			return null;
		}
	}
	
	// Convention: return null on error, return JSONArray on success
	public static JSONArray getArray(String url, List<NameValuePair> params) {
		String json = httpRequest(url, params);
		if(json == null) { return null; }
		try {
			// getArray automatically updates the statically declared private jArray
			jArray = null;
			jArray = new JSONArray(json);
			return jArray;
		}
		catch (JSONException e) {
			Log.e("Json.getArray", "Error: " +e.toString());
			return null;
		}
	}
	
	// Convention: return null on error, return String on success
	private static String httpRequest(String url, List<NameValuePair> params) {
		InputStream is;
		String json = null;
		try {
			DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpPost httpPost = new HttpPost(url);
			httpPost.setEntity(new UrlEncodedFormEntity(params));
			HttpResponse httpResponse = httpClient.execute(httpPost);
			HttpEntity httpEntity = httpResponse.getEntity();
			is = httpEntity.getContent();
			
			BufferedReader reader = new BufferedReader(new InputStreamReader(is, "iso-8859-1"), 8);
			StringBuilder sb = new StringBuilder();
			String line = null;
			
			while ((line = reader.readLine()) != null)
				sb.append(line + "\n");
			
			is.close();
			json = sb.toString();
		} catch (Exception e) {
			Log.e("Common.Server.getJSONString", "Error: " +e.toString());
			return null;
		}
		return json;
	}
	
	public static int arrayLength() {
		if(jArray != null)
			return jArray.length();
		else
			return 0;
	}
	
	public static void printArray(Context context, TableLayout searchtable) {
		
		TableRow.LayoutParams lp = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.MATCH_PARENT, 2f);
		TableRow.LayoutParams searchtable_lp = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.MATCH_PARENT, 2f);
		lp.setMargins(4, 2, 2, 2);//2px right-margin
		searchtable_lp.setMargins(8, 2, 8, 2);//2px right-margin
		searchtable.setColumnShrinkable(0, true);
		searchtable.setLayoutParams(searchtable_lp);
		
		try{
			for (int j = 0; j < jArray.length(); j++) {
				JSONObject obj = jArray.getJSONObject(j);
				final String Opstelpunt = obj.getString("Opstelpunt");
				final String Status = obj.getString("Status");
				final String Network = obj.getString("Network");
				final String Reason = obj.getString("Reason");
				final String date_start = obj.getString("date_start");
				final String date_end = obj.getString("date_end");
				final String Submitter = obj.getString("Submitter");
				final String Description = obj.getString("Description");
	 			
	 			//row
	 			final TableRow mainRow= new TableRow(context); //rand cu info de baza
 				final TableRow detailRow= new TableRow(context); //rand cu detalii
 				
// 				mainRow.setLayoutParams(lp_row);
// 				detailRow.setLayoutParams(lp_row);
 				
 				TextView mainTV_left = new TextView(context);//site_nr+date_start
 				TextView mainTV_right = new TextView(context);//status
 				
 				mainTV_left.setTextColor(Color.BLACK);
 				mainTV_right.setTextColor(Color.BLACK);
 				//mainTV_right.setTypeface(null, Typeface.BOLD);
 				set_status_color(Status, mainTV_right); //open-magenta,new-blue,closed-black
 				final TextView detailsTV = new TextView(context);
 				
 				String data = date_start.substring(0, 10); // pt afisarea datei fara ora
 				LinearLayout cell1 = createCell(mainTV_left, " Site: "+Opstelpunt+'\n'+" Start date: "+data, lp, 14, Gravity.CENTER_VERTICAL|Gravity.LEFT, context);
 				LinearLayout cell2 = createCell(mainTV_right,Status+"  ", lp, 14, Gravity.CENTER_VERTICAL|Gravity.RIGHT, context);
 				
 				mainRow.addView(cell1);//contine tv1
 				mainRow.addView(cell2);//contine tv2
 				//mainrow.setBackgroundResource(R.drawable.bar_bg);
 				mainRow.setBackgroundResource(R.color.row_color);
// 				mainRow.setBackgroundColor(Color.CYAN);
 				
 				//final LinearLayout detailCell = create_col (ind, Opstelpunt, lp, 0, Gravity.LEFT | Gravity.TOP);
 				final LinearLayout detailCell = new LinearLayout(context);
 				detailCell.setLayoutParams(lp);//2px border on the right for the cell
 				detailsTV.setPadding(0, 0, 0, 0);
 				detailsTV.setLayoutParams(lp);
 				detailsTV.setVisibility(View.GONE);
// 				detailsTV.setTextSize((float) 0.1);
 				Log.i("--------------", ""+detailsTV.getTextSize());
 				detailsTV.setGravity(Gravity.LEFT | Gravity.TOP);
 				detailCell.addView(detailsTV);
 				detailRow.addView(detailCell); //rand cu un textview ce va contine detaliile
 			
 				searchtable.addView(mainRow);
 				searchtable.addView(detailRow); //pentru detalii
 		       	
 				mainRow.setClickable(true);
 				mainRow.setOnClickListener(new OnClickListener() {
 					@Override
 					public void onClick(View v) {
 						if(detailsTV.getVisibility()==View.GONE) {
 							detailsTV.setVisibility(View.VISIBLE);
 				        	detailsTV.setTextColor(Color.BLACK);
 				        	detailsTV.setText("Time: "+date_start+ "  -  "+date_end+'\n'+
	 				        	"Submitter: "+Submitter+'\n'+
	 				        	"Systems: "+Network+'\n'+
	 				        	"Reason: "+Reason+'\n'+'\n'+
	 				        	"Description:" +'\n'+
	 				        	Description);
 				        }
 					    else {
 				        	detailsTV.setVisibility(View.GONE);
 	 					}
 					}
 	 			});    	
	 		}
	    } catch(Exception e) {
	    	Log.e("log_tag1", "Error " +e.toString());
	    }
	}
	
	private static void set_status_color(String var, TextView tv) {
		if(var.equals("Closed")) {
			tv.setTextColor(Color.BLACK);
		}
		else if(var.equals("Open")) {
			tv.setTextColor(Color.rgb(0, 200, 0)); //green
			tv.setTypeface(null, Typeface.BOLD);
		}
		else {
			//tv.setTextColor(Color.BLUE);
			tv.setTextColor(Color.rgb(0, 0, 200)); //blue
			tv.setTypeface(null, Typeface.BOLD);
		}
	}
	
	private static LinearLayout createCell(TextView cellName, String name, TableRow.LayoutParams lp, Integer textsize, int gravity, Context context) {
		LinearLayout cell = new LinearLayout(context);
		cell.setLayoutParams(lp); // 2px border on the right for the cell
		cellName.setLayoutParams(lp);
		cellName.setText(name);
		cellName.setGravity(Gravity.CENTER_VERTICAL|Gravity.CENTER);
		cellName.setPadding(0, 0, 0, 0);
		cell.addView(cellName);
		cellName.setTextSize(textsize);
		cellName.setGravity(gravity);
		return cell;
	}
}
