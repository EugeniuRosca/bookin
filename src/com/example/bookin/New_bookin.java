package com.example.bookin;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class New_bookin extends Activity implements AsyncTaskCallbacks {
	
	private String opstelpunt, network, reason, ticket, descUser, status;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.newbookin);
	}
	
	private void setMessageColor(int color) {
		((TextView)findViewById(R.id.newbookin_message)).setTextColor(color);
	}
	
	public void submit_newbookin_function(View v) {
		opstelpunt = ((EditText) findViewById(R.id.newbookin_sitenr)).getText().toString();
		network = 	((Spinner) findViewById(R.id.newbookin_snetwork)).getSelectedItem().toString();
		reason = 	((Spinner) findViewById(R.id.newbookin_sactivity)).getSelectedItem().toString();
		ticket = 	((EditText)findViewById(R.id.newbookin_ticketnr)).getText().toString();
		descUser = 	((EditText) findViewById(R.id.newbookin_description)).getText().toString();
		status = 	"New";
		
		if( (opstelpunt.matches("")) 				||
			(network.matches("Choose network")) 	||
			(reason.matches("Choose activity")) 	||
			(ticket.matches(""))					||
			(descUser.matches(""))) {
			this.setMessageColor(Color.RED);
			return;
		}
		
		// First request is about getting details about user company and phone
		// Second request (of actual pushing the new bookin) will be done later in doInBg()
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("username", User.name));
		Server.startAsync(this, params, Url.userdetails, ConnectionType.USER_DETAILS);
	}
	
	private void hideKeyboard() {
		InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
 		imm.hideSoftInputFromWindow(getWindow().getCurrentFocus().getWindowToken(), 0);
	}
	
	private void printToast(String msg) {
		Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
	}
	
	private List<NameValuePair> buildParamForSecondRequest(ServerReply reply) {
		// Get phone and company from the server message
		String phone = reply.info.get(Tag.PHONE);
		String company = reply.info.get(Tag.COMPANY);
		
		String descServer = "tel. "+ phone +" ("+ company +"), TT"+ this.ticket;
		String description = descServer +"\n"+ this.descUser;
		
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("Opstelpunt", this.opstelpunt));
		params.add(new BasicNameValuePair("Status", this.status));
		params.add(new BasicNameValuePair("Network", this.network));
		params.add(new BasicNameValuePair("Reason", this.reason));
		params.add(new BasicNameValuePair("Submitter", User.name));
		params.add(new BasicNameValuePair("Description", description));
		return params;
	}
	
	public String doInBg(AsyncParam p) {
		ServerReply reply = Server.sendReceive(p.url, p.msg, p.connType);
		if(reply.error != null)
			return reply.error;
		
		// Using phone & company (got from server) and description (got from user)
		// build the full description for the new bookin
		List<NameValuePair> params = this.buildParamForSecondRequest(reply);
		
		// Initiate a second request to server (of NEW_BOOKIN type)
		reply = Server.sendReceive(Url.newbookin, params, ConnectionType.NEW_BOOKIN);
		return reply.error;
	}

	public void postExec(String error) {
		this.hideKeyboard();
		if(error != null) {
			this.printToast(error);
			return;
		}
		String message = "You are now booked in on site "+ this.opstelpunt +"!" +
				"\nCheck your bookins in a few minutes \nfor acceptance from FO";
		this.printToast(message);
		this.finish(); // terminate New_bookin and go back to Menu
	}
}