package com.example.bookin;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class Change_password extends Activity implements AsyncTaskCallbacks {
	private String newpass;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.change_pass);
		
		TextView message = (TextView)findViewById(R.id.changepass_message);
		message.setText("username: "+User.name);
	}
	
	public void changepass_function(View v) {
		EditText oldpasscode = (EditText)findViewById(R.id.changepass_oldpass);
		EditText newpasscode = (EditText)findViewById(R.id.changepass_newpass);
		EditText newpasscode2 = (EditText)findViewById(R.id.changepass_newpass2);
		
		String oldpass = oldpasscode.getText().toString();
		this.newpass = newpasscode.getText().toString();
		String newpass2 = newpasscode2.getText().toString();
		String errorMsg = null;
		
		if(!oldpass.equals(User.pass))
			errorMsg = "Wrong password!";
		else if(!this.newpass.equals(newpass2))
			errorMsg = "New password doesn't match!";
		else if(this.newpass.length() < 4)
			errorMsg = "New password is too short."+"\n"+"Use minimum 5 characters!";
		
		if(errorMsg != null) {
			this.printToast(errorMsg);
			return;
		}
		
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("username", User.name));
		params.add(new BasicNameValuePair("passcode", User.pass));
		params.add(new BasicNameValuePair("newpasscode", this.newpass));
		Server.startAsync(this, params, Url.changepassword, ConnectionType.CHANGE_PASSWORD);
	}
	
	private void hideKeyboard() {
		InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
 		imm.hideSoftInputFromWindow(getWindow().getCurrentFocus().getWindowToken(), 0);
	}
	
	private void printToast(String param) {
		Toast.makeText(this,  param, Toast.LENGTH_LONG).show();
	}
	
	public String doInBg(AsyncParam p) {
		ServerReply reply = Server.sendReceive(p.url, p.msg, p.connType); 
		return reply.error;
	}

	public void postExec(String error) {
		Log.i("postExec_Change_password", "entered: "+error);
		this.hideKeyboard();
		if(error == null) {
			this.printToast("Password changed successfully!");
			User.pass = this.newpass;
			this.finish(); // FIXME: decide if this is expected behavior (auto-go to Main_login)
		}
		else
			this.printToast(error);
		Log.i("postExec_Change_password", "exited");
	}
}