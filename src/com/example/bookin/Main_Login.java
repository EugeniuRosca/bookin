package com.example.bookin;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import android.os.Bundle;
import android.os.StrictMode;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

public class Main_Login extends Activity implements AsyncTaskCallbacks {
	
	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
	       .detectAll()
	       .build());
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main_login);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main_login, menu);
		return true;
	}
	
	public void login_function(View v) {
		Log.i("login_function", "enter");
		
		this.clearMessage();
		EditText username = (EditText)findViewById(R.id.login_user);
		EditText passcode = (EditText)findViewById(R.id.login_pass);
		User.name = username.getText().toString();
		User.pass = passcode.getText().toString();
		
		String msg = null;
		if(User.name.equals("") && User.pass.equals(""))
			msg = "Insert"+'\n' +"username & password!";
		else if(User.name.equals(""))
			msg = "Insert username!";
		else if(User.pass.equals(""))
			msg = "Insert password!";
		
		if(msg != null) {
			this.printMessage(msg);
			return;
		}
		
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("username", User.name));
		params.add(new BasicNameValuePair("passcode", User.pass));
		Server.startAsync(this, params, Url.login, ConnectionType.MAIN_LOGIN);
		Log.i("login_function", "exit");
	}
	
	public void signup_function(View v) {
	    Intent intent = new Intent(this, Signup.class);
	    Main_Login.this.startActivity(intent);
	}
	
	public void forgot_function(View v) {
		Intent intent = new Intent(this, Forgot_userpass.class);
		Main_Login.this.startActivity(intent);
	}

	public void changepassword_function(View v){
		Intent intent = new Intent(this, Change_password.class);
	    Main_Login.this.startActivity(intent);
	}
	
	private void printMessage(String param) {
		TextView message = (TextView)findViewById(R.id.login_message);
		message.setText(param);
	}
	
	private void clearMessage() { this.printMessage(""); }
	
	private void hideKeyboard() {
		InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
 		imm.hideSoftInputFromWindow(getWindow().getCurrentFocus().getWindowToken(), 0);
	}
	
	public String doInBg(AsyncParam p) {
		ServerReply reply = Server.sendReceive(p.url, p.msg, p.connType); 
		return reply.error;
	}
	
	public void postExec(String error) {
		Log.i("postExec_Main_Login", "entered: "+error);
		this.hideKeyboard();
		if(error == null) { // success !
			Intent intent = new Intent(getApplicationContext(), com.example.bookin.Menu.class);
		    this.startActivity(intent);
		    this.finish();
		}
		else {
			this.printMessage(error);
		}
		Log.i("postExec_Main_Login", "exited");
	}
}