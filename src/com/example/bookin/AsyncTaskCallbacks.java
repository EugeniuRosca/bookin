package com.example.bookin;

public interface AsyncTaskCallbacks {
	public String doInBg(AsyncParam param);
	public void postExec(String param);
}