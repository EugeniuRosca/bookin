package com.example.bookin;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

public class My_bookins extends Activity implements AsyncTaskCallbacks {

	private Spinner spinner;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.my_bookins);

		spinner = (Spinner)findViewById(R.id.mybookins_status);
		ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
		        R.array.mybookins_status, android.R.layout.simple_spinner_item);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinner.setAdapter(adapter); // Apply the adapter to the spinner
		spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				mybookinsSpinnerSelectFunction();
			}
			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
			}
		});
	}
	
	public void mybookinsRefreshFunction(View v) {
		mybookinsSpinnerSelectFunction();
		// TODO: Display "Refreshed" (only when NO connection issues!)
	}
	
	public void mybookinsSpinnerSelectFunction() {
		String url = "";
		String sp = spinner.getSelectedItem().toString();
		
		if(sp.equals("All my bookins"))
			url = Url.myallbookins;
		else if(sp.equals("New + Open"))
			url = Url.mynewopenbookins;
		else
			url = Url.mybookins;

		mybookinsSearchFunction(url, sp);
	}
	
	public void mybookinsSearchFunction(String url, String status) {
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("Submitter", User.name));
		params.add(new BasicNameValuePair("Status", status));
		Server.startAsync(this, params, url, ConnectionType.MY_BOOKINS);
	}
	
	private void printResultCount(int num) {
		TextView nr_of_results = (TextView)findViewById(R.id.mybookins_nr_of_results);
		if(num == 1) {
			nr_of_results.setText("1 result");
		}
		else {
			nr_of_results.setText(num+" results");
		}			
	}
	
	private void printToast(String msg) {
		Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
	}
	
	private void printTableEntries() {
		TableLayout searchtable = (TableLayout) findViewById(R.id.mybookins_table);
		searchtable.removeAllViews();
		if(Json.arrayLength() == 0)
			this.printToast("No records found!");
		else
			Json.printArray(this, searchtable);
	}
	
	public String doInBg(AsyncParam p) {
		ServerReply reply = Server.sendReceive(p.url, p.msg, p.connType); 
		return reply.error;
	}
	
	public void postExec(String error) {
		Log.i("postExec_My_bookins", "entered with param: "+error);
		if(error != null) {
			this.printToast(error);
			return;
		}
		this.printResultCount(Json.arrayLength());
		this.printTableEntries();
		Log.i("postExec_My_bookins", "exited");
	}
}