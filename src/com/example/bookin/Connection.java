package com.example.bookin;

import java.net.URL;
import java.net.URLConnection;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

enum ConnectionType {
	CHANGE_PASSWORD,
	MAIN_LOGIN,
	MY_BOOKINS,
	NEW_BOOKIN,
	SIGN_UP,
	SITE_SEARCH,
	USER_DETAILS
}

enum ConnectionState {
	CONNECTION_OK,
	INTERNET_DOWN,
	SERVER_DOWN
}

public class Connection {
	private Context context;
	
	public Connection(Context ctx) {
		this.context = ctx;
	}
	
	public ConnectionState check() {
		if(connectionDown())
			return ConnectionState.INTERNET_DOWN;
		if(serverDown(Url.server, 3000))
			return ConnectionState.SERVER_DOWN;
		return ConnectionState.CONNECTION_OK;
	}
	
	private boolean connectionDown() {
		ConnectivityManager connManager = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo networkInfo = connManager.getActiveNetworkInfo();
		if(networkInfo != null && networkInfo.isConnected())
			return false;
		else
			return true;
	}
	
	private boolean serverDown(String url, int timeout) {
		try {
	        URL myUrl = new URL(url);
	        URLConnection connection = myUrl.openConnection();
	        connection.setConnectTimeout(timeout);
	        connection.connect();
	        return false;
	    } catch (Exception e) {
	    	return true;
	    }
	}
}
